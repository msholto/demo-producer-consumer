package sk.sholto.demo.producerconsumer.model.def;

public enum EventType {
    ADD_USER,
    PRINT_ALL,
    DELETE_ALL
}
