package sk.sholto.demo.producerconsumer.model.dto.event;

public record AddUserEventDto(Long userId,
                              String userGuid,
                              String userName) {
}
