package sk.sholto.demo.producerconsumer.model.dto.event;

import sk.sholto.demo.producerconsumer.model.def.EventType;

public record EventDto(EventType eventType,
                       Object eventData) {
}
