package sk.sholto.demo.producerconsumer.model.entity;

public record UserEntity(Long userId,
                         String userGuid,
                         String userName) {
}
