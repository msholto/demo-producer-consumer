package sk.sholto.demo.producerconsumer.service;

import sk.sholto.demo.producerconsumer.listener.ConsumerListener;
import sk.sholto.demo.producerconsumer.model.def.EventType;

public interface EventService extends Runnable, AutoCloseable {

    boolean sendEvent(final EventType eventType,
                      final Object data);

    void registerConsumerListener(final EventType eventType,
                                  final ConsumerListener consumerListener);
}
