package sk.sholto.demo.producerconsumer.service;

public interface DatabaseService {

    void addUser(final Long userId,
                 final String userGuid,
                 final String userName);

    void deleteAll();

    void printAll();
}
