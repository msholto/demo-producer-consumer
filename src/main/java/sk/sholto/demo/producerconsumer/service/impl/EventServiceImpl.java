package sk.sholto.demo.producerconsumer.service.impl;

import sk.sholto.demo.producerconsumer.listener.ConsumerListener;
import sk.sholto.demo.producerconsumer.model.def.EventType;
import sk.sholto.demo.producerconsumer.model.dto.event.EventDto;
import sk.sholto.demo.producerconsumer.model.exception.EventTypeMissingException;
import sk.sholto.demo.producerconsumer.service.EventService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class EventServiceImpl implements EventService {

    private final Queue<EventDto> queue;
    private final ExecutorService executorService;
    private final Map<EventType, List<ConsumerListener>> consumerListeners;

    private boolean isRunning;

    public EventServiceImpl() {
        this.queue = new LinkedBlockingQueue<>();
        this.executorService = Executors.newFixedThreadPool(10);

        // This will be created only once, so thread safety is not necessary
        this.consumerListeners = new HashMap<>();

        // Now init the map with potential consumer listeners for speed right now
        for (int i = 0; i < EventType.values().length; i++) {
            this.consumerListeners.put(EventType.values()[i],
                                       Collections.synchronizedList(new ArrayList<>()));
        }
    }

    @Override
    public boolean sendEvent(final EventType eventType,
                             final Object data) {
        if (!this.isRunning) {
            return false;
        }

        if (eventType == null) {
            throw new EventTypeMissingException();
        }

        final EventDto eventDto = new EventDto(eventType,
                                               data);

        System.out.println("Sending: " + eventDto);
        return this.queue.offer(eventDto);
    }

    @Override
    public void registerConsumerListener(final EventType eventType,
                                         final ConsumerListener consumerListener) {
        System.out.println("Registering consumer listener: " + eventType);

        this.consumerListeners.get(eventType).add(consumerListener);

        System.out.println("Consumer listener registered: " + eventType);
    }

    @Override
    public void run() {
        this.isRunning = true;

        while (this.isRunning) {
            final EventDto eventDto = this.queue.poll();

            if (eventDto == null) {
                continue;
            }

            System.out.println("Processing: " + eventDto);

            final List<ConsumerListener> eventConsumerListeners = this.consumerListeners.get(eventDto.eventType());

            System.out.println("Invoking consumer listeners: " + eventConsumerListeners.size());
            /*eventConsumerListeners.forEach(eventConsumerListener -> executorService.submit(() -> eventConsumerListener.receiveEvent(eventDto.eventData())));*/

            // Let's do it in sync
            eventConsumerListeners.forEach(eventConsumerListener -> eventConsumerListener.receiveEvent(eventDto.eventData()));
        }
    }

    @Override
    // We can do shut down via event if we want
    public void close() throws Exception {
        System.out.println("Closing EventServiceImpl");

        this.isRunning = false;

        executorService.shutdownNow();

        if (!executorService.awaitTermination(10,
                                              TimeUnit.SECONDS)) {
            System.out.println("Executor termination failed");
        } else {
            System.out.println("Executor terminated");
        }
    }
}
