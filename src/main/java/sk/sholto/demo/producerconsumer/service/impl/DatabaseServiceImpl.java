package sk.sholto.demo.producerconsumer.service.impl;

import sk.sholto.demo.producerconsumer.model.entity.UserEntity;
import sk.sholto.demo.producerconsumer.service.DatabaseService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DatabaseServiceImpl implements DatabaseService {

    // Instead of embedded database
    private final Map<Long, UserEntity> users;

    public DatabaseServiceImpl() {
        this.users = new ConcurrentHashMap<>();
    }

    @Override
    public void addUser(final Long userId,
                        final String userGuid,
                        final String userName) {
        System.out.println("Adding user " + userId + " with guid: " + userGuid);

        this.users.put(userId,
                       new UserEntity(userId,
                                      userGuid,
                                      userName));
    }

    @Override
    public void deleteAll() {
        System.out.println("Deleting all users");

        this.users.clear();
    }

    @Override
    public void printAll() {
        System.out.println("Printing all users");

        this.users.forEach((userId, userEntity) -> System.out.println(userEntity));
    }

    protected Map<Long, UserEntity> getUsers() {
        return users;
    }
}
