package sk.sholto.demo.producerconsumer;

import sk.sholto.demo.producerconsumer.model.def.EventType;
import sk.sholto.demo.producerconsumer.model.dto.event.AddUserEventDto;
import sk.sholto.demo.producerconsumer.service.DatabaseService;
import sk.sholto.demo.producerconsumer.service.EventService;
import sk.sholto.demo.producerconsumer.service.impl.DatabaseServiceImpl;
import sk.sholto.demo.producerconsumer.service.impl.EventServiceImpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ProducerConsumerApplication {

    public static void main(String[] args) {
        final DatabaseService databaseService = new DatabaseServiceImpl();

        try (final ExecutorService executorService = Executors.newSingleThreadExecutor();
             final EventService eventService = new EventServiceImpl()) {
            eventService.registerConsumerListener(EventType.ADD_USER,
                                                  data -> {
                                                      final AddUserEventDto addUserEventDto = (AddUserEventDto) data;

                                                      databaseService.addUser(addUserEventDto.userId(),
                                                                              addUserEventDto.userGuid(),
                                                                              addUserEventDto.userName());
                                                  });
            eventService.registerConsumerListener(EventType.PRINT_ALL,
                                                  data -> databaseService.printAll());
            eventService.registerConsumerListener(EventType.DELETE_ALL,
                                                  data -> databaseService.deleteAll());

            executorService.submit(eventService);

            eventService.sendEvent(EventType.ADD_USER,
                                   new AddUserEventDto(1L,
                                                       "a1",
                                                       "Robert"));
            eventService.sendEvent(EventType.ADD_USER,
                                   new AddUserEventDto(2L,
                                                       "a2",
                                                       "Martin"));
            eventService.sendEvent(EventType.PRINT_ALL,
                                   null);
            eventService.sendEvent(EventType.DELETE_ALL,
                                   null);
            eventService.sendEvent(EventType.PRINT_ALL,
                                   null);

            executorService.awaitTermination(10,
                                             TimeUnit.SECONDS);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

}
