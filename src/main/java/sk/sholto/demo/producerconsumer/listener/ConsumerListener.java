package sk.sholto.demo.producerconsumer.listener;

public interface ConsumerListener {

    void receiveEvent(final Object data);
}
