package sk.sholto.demo.producerconsumer.service.impl;

import org.hamcrest.MatcherAssert;
import org.testng.annotations.Test;
import sk.sholto.demo.producerconsumer.model.entity.UserEntity;

import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class DatabaseServiceImplTest {

    @Test
    public void testAddUser() {
        final DatabaseServiceImpl databaseService = new DatabaseServiceImpl();
        databaseService.addUser(1L,
                                "a1",
                                "Something");

        final Map<Long, UserEntity> users = databaseService.getUsers();

        MatcherAssert.assertThat(users,
                                 is(notNullValue()));
        MatcherAssert.assertThat(users.size(),
                                 is(equalTo(1)));

        final UserEntity userEntity = users.get(1L);

        MatcherAssert.assertThat(userEntity,
                                 is(notNullValue()));
        MatcherAssert.assertThat(userEntity.userId(),
                                 is(equalTo(1L)));
        MatcherAssert.assertThat(userEntity.userGuid(),
                                 is(equalTo("a1")));
        MatcherAssert.assertThat(userEntity.userName(),
                                 is(equalTo("Something")));
    }
}